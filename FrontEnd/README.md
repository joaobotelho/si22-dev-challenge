#Frontend

## Choosing frontend tech

For this project, it was chosen to be used **React**, with the support of **axios** to handle the API requests, and **react-bootstrap** to support on the design.

## Components

Tables where used to display the information of both drinks and cocktails, so, firstly it was created a component to deal with the display of both of them.

Following was created the delete component, which simply makes an api request to delete the cocktail given as argument when the button it displays is clicked.

Next, the search components where created, where they receive a word through a form, passing it to the API, and displaying the results.

Now, with all display components made, the focus shifted to the update components, where it gives the option to update the stock of any given drink. Just as the last, it was done through passing a form value to the API, and displaying it back. The same was done to the creation of the drink creating component.

With the drink components finished, it was time to return to the cocktails 🍷. To display the creation menu it was used a toggle button, which displays a blocked button, a button to hide the menu, and three forms to be filled: the cocktail name, and two forms for adding ingredients. This menu also has a button to add more ingredient forms.

The button to add more ingredients was implemented creating a list of ingredient form components, and displaying this list directly.
