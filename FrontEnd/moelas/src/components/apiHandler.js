import axios from 'axios'
import React from 'react'
import {format} from 'react-string-format'

const drink = axios.create({baseURL: "http://localhost:3000/drinks"})
const cocktail = axios.create({baseURL: "http://localhost:3000/cocktails"})

export const GetCocktails = () => {
    const [cocktails, setCocktails] = React.useState([]);

    React.useEffect(() => {
        let mounted = true;
        cocktail.get().then((response) => response.data).then((items) => {
            if (mounted){
                setCocktails(items);
            }
        })
    }, []);
    return cocktails;
}

export const BuyCocktail = async (slug) => {
    const response = await cocktail.get(format('/buy/{0}', slug))
    return response
}

export const DeleteCocktail = async (slug) => {
    const response = await cocktail.delete(slug)
    return response
}

export const GetDrinks = async () => {
    const response = await drink.get()
    return response.data.data
}

export const DeleteDrink = async (slug) => {
    const response = await drink.delete(slug)
    return response
}

export const UpdateDrinkStock = async (slug, stock) => {
    const response = await drink.patch(format('/{0}', slug), {stock: stock})
    return response
}

export const SearchDrinks = async (name) => {
    const formatted_string = format('http://localhost:3000/drinks/search/{0}', name)
    const response = await axios.get(formatted_string)
    return response
}

export const SearchCocktails = async (name) => {
    const formatted_string = format('http://localhost:3000/cocktails/search/{0}', name)
    const response = await axios.get(formatted_string)
    return response
}


export const CreateDrink = (name, stock, img_url) => {
    drink.post('/', {
        name: name,
        stock: stock,
        img_url: img_url
    })
}

export const CreateCocktail = async (obj) => {
    const response = await cocktail.post('/', obj);
    return response
}