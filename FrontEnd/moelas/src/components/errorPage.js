import React from 'react'

function ErrorPage() {
  return (
    <div style={{paddingTop: '20px'}}>
        Invalid page path...
    </div>
  )
}

export default ErrorPage
