import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Container from 'react-bootstrap/Container';

export const AppNavbar = () => {
    return (
        <Navbar bg="dark" variant="dark" fixed="top" id="navbar">
            <Container>
                <Navbar.Brand href="/drinks">O Moelas</Navbar.Brand>
                <Nav className="me-auto">
                    <Nav.Link href="/drinks">Drinks</Nav.Link>
                    <Nav.Link href="/cocktails">Cocktails</Nav.Link>
                </Nav>
            </Container>
        </Navbar>        
    )
}

export default AppNavbar