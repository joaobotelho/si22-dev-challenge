import {useState} from 'react'
import Form from 'react-bootstrap/Form'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Button from 'react-bootstrap/Button'
import Col from 'react-bootstrap/esm/Col'
import {SearchCocktails} from '../apiHandler'
import CocktailTable from './cocktailTable'

export const SearchCocktail = (props) => {
    const [search, setSearch] = useState([])
    const [cocktail, setCocktail] = useState({name: ''})
    const [toggle, setToggle] = useState(false);
    
    const handleSubmit = () => {
        setToggle(true);
        const data = SearchCocktails(cocktail.name);
        data.then((result) => {
            if (result.data.notice){
                setToggle(false);
            }
            setSearch(result.data)
        })

        console.log(search);
    }

    return (
        <Container fluid style={{paddingTop: '20px'}}>
            <Row>
                <Col xs={9}>
                    <Form.Group className="mb-3" controlId="searchCocktail.name" value = {cocktail.name} 
                        onChange={e => setCocktail({name: e.target.value})}>
                        <Form.Control type="text" placeholder="Search" />
                    </Form.Group>    
                </Col>
                <Col>
                    <Button variant="primary" onClick={handleSubmit}>Search</Button>
                </Col>
                <Col/>
            </Row>
            <Row>
                {(toggle && Array.isArray(search)) ? <CocktailTable cocktails={search}/>
                : <p> {search.notice} </p>    
            }
            </Row>
        </Container>
    )
}

export default SearchCocktail