import Form from 'react-bootstrap/Form'
import {GetDrinks} from '../apiHandler'
import React from 'react'
import Button from 'react-bootstrap/Button'

export const FormIngredient = (props) => {

    const [drinks, setDrinks] = React.useState([]);

    const [added, setAdded] = React.useState(false);

    const [param, setParam] = React.useState({
        drink: "",
        amount: 0
    });

    const handleClick = () => {
        setAdded(true);
        props.setInput(param)
    }

    React.useEffect(() => {
        const drinkAPI = GetDrinks();
        let mounted = true;
        drinkAPI.then((items) => {
            if (mounted) {
            setDrinks(items);
            }
        })
    }, []);

    return (
        <Form>
            <Form.Group className="mb-3">
                <Form.Label>
                    Ingredient
                </Form.Label>
                <Form.Control as="select" value = {param.drink} onChange={e => {setParam({
                    drink: e.target.value, amount: param.amount
                })}}>
                    {drinks.map( drink => {
                        return (
                            <option>{drink.attributes.slug}</option>
                        )
                    })}
                </Form.Control>
            </Form.Group>
            <Form.Group className="mb-3">
                <Form.Label>Amount</Form.Label>
                <Form.Control type="number" value = {param.amount} onChange={e => {
                    setParam({drink: param.drink, amount: e.target.value})
                }}/>
            </Form.Group>
            {added ? 
            <Button variant="outline-primary" disabled>Added</Button> : 
            <Button variant="outline-primary" type="submit" onClick={handleClick}>Add</Button>}
            
        </Form>
        
    )
}

export default FormIngredient