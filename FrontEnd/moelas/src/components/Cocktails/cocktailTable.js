import Table from 'react-bootstrap/Table';
import Container from 'react-bootstrap/Container';
import {useState} from 'react'
import Buy from './buy'
import Alert from 'react-bootstrap/Alert'
import Button from 'react-bootstrap/Button'
import Delete from './delete';
import Accordion from 'react-bootstrap/Accordion'

export const CocktailTable = (props) => {
    const [notice, setNotice] = useState([{}]);
    const [show, setShow] = useState(false) 
    return (
        <Container className="justify-content-md-center" style={{paddingTop: '20px'}}>      
        {show && 
        <Alert variant={notice.error ? 'danger': 'success'}>
            <Alert.Heading>{notice.error? notice.error : notice.notice}</Alert.Heading>    
            <Button variant="outline-info" onClick={() => {setShow(false)}}>Hide</Button>
        </Alert>}
        <Table>
            <thead>
                <tr>
                    <th>cocktail</th>
                    <th>Buy</th>
                    <th>Delete</th>
                    <th>Ingredients</th>
                </tr>
            </thead>
            <tbody>
                {props.cocktails.map( obj => {
                    return(
                        <tr key={obj.cocktail.id}>
                            <td>{obj.cocktail.name}</td>
                            <td><Buy setNotice={setNotice} slug={obj.cocktail.slug} setShow={setShow}/></td>
                            <td><Delete setNotice={setNotice} slug={obj.cocktail.slug} setShow={setShow}></Delete></td>
                            <td>
                                <Accordion defaultActiveKey="0" flush>
                                    <Accordion.Header eventKey="0">See Ingredients</Accordion.Header>
                                    <Accordion.Body>
                                        {
                                            obj.ingredients.map(
                                                ingredient => {
                                                    return (
                                                        <div key={ingredient.name}>
                                                            {ingredient.amount}x {ingredient.drink}
                                                        </div>          
                                                    )
                                                }
                                            )
                                        }
                                    </Accordion.Body>
                                </Accordion>
                            </td>
                        </tr>
                    )
                })}
            </tbody>
        </Table>
    </Container>
    )

}

export default CocktailTable