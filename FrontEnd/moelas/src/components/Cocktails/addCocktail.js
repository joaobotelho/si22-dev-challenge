import {useState} from 'react'
import Button from 'react-bootstrap/Button'
import Container from 'react-bootstrap/Container'
import FormIngredient from './formIngredient'
import Form from 'react-bootstrap/Form'
import Row from 'react-bootstrap/Row'
import {CreateCocktail} from '../apiHandler'

export const AddCocktail= () => {
    const [show, setShow] = useState(false);
    
    const [input, setInput] = useState([]);

    const [cocktail, setCocktail] = useState({
        name: "",
        ingredients: []
    });

    const updateInput = (obj) => {
        setCocktail({
            name: cocktail.name,
            ingredients: [...cocktail.ingredients, obj]
        })
    }

    const handleSubmit = () => {
        const response = CreateCocktail(cocktail);
        console.log(response);
        const data = response.then((resp) => resp.data)
        console.log(data);
    }

    const handleClick = () => {
        if (!show)
        {   
            setShow(true);
        }
        else
        {
            setShow(false);
        }
    }

    const addOnClick = () => {
        setInput(input.concat(<FormIngredient setInput={updateInput}/>))
    }

    if (show) {
        return (
            <Container>
                <Form onSubmit={handleSubmit}>
                    <Form.Group className="mb-3" value={cocktail.name}
                    onChange = {e => setCocktail({name: e.target.value, ingredients: cocktail.ingredients})}>
                        <Form.Label>Name</Form.Label>
                        <Form.Control type="text"/>
                        
                    </Form.Group>

                    {(cocktail.ingredients.length >= 2 && cocktail.name !== '')?
                        <Button variant="outline-primary" type="submit">Create Drink</Button> :
                        <Button variant="outline-primary" type="submit" disabled>Create Drink</Button>
                    }
                    
                </Form>
                <p/>

                <Row>
                        <FormIngredient setInput={updateInput} input={input}/>
                        <FormIngredient setInput={updateInput}/>
                        {input}    
                    </Row>
                    <Button variant="outline-primary" onClick={addOnClick}>Add Ingredient</Button>
                <p/>
                <Button variant="outline-primary" type="submit" onClick={handleClick}>Hide</Button>
            </Container>
        )
    }
    return (
        <Button onClick={handleClick}>Add new Cocktail</Button>
    )
}

export default AddCocktail