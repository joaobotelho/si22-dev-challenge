import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import {BuyCocktail} from '../apiHandler'

export const Buy = (props) => {
    const buyHandler = () => {
        const response = BuyCocktail(props.slug);
        response.then((resp) => {props.setNotice(resp.data)})
        props.setShow(true);
    }

    return (
        <Button variant="outline-success" type="submit" onClick={buyHandler}>Buy</Button>
    )
}

export default Buy