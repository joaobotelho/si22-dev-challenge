import Container from 'react-bootstrap/Container'
import {GetCocktails} from '../apiHandler'
import CocktailTable from './cocktailTable'
import AddCocktail from './addCocktail'
import SearchCocktail from './searchCocktail'

export const Cocktails = () => {
    return (
        <Container style={{paddingTop: "20px"}}>
            <SearchCocktail/>
            <AddCocktail/>
            <CocktailTable cocktails={GetCocktails()} />
        </Container>
    )
}

export default Cocktails