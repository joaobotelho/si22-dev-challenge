import Button from "react-bootstrap/esm/Button"
import { DeleteCocktail } from "../apiHandler"

export const Delete = (props) => {

    const handleDelete = () => {
        const response = DeleteCocktail(props.slug);
        response.then((resp) => {props.setNotice(resp.data)})
    }

    return (
        <Button variant="outline-danger" onClick={handleDelete}>Delete</Button>
    )
}

export default Delete