import {useState} from 'react'
import Form from 'react-bootstrap/Form'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Button from 'react-bootstrap/Button'
import Col from 'react-bootstrap/esm/Col'
import {SearchDrinks} from '../apiHandler'
import DrinkTable from './drinkTable'

export const SearchDrink = (props) => {
    const [search, setSearch] = useState([])
    const [drink, setDrink] = useState({name: ''})
    const [toggle, setToggle] = useState(false);
    
    const handleSubmit = () => {
        setToggle(true);
        const data = SearchDrinks(drink.name);
        data.then((result) => {
            if (!result.data.data){
                setToggle(false);
            }
            setSearch(result.data.data)
        })
    }

    return (
        <Container fluid style={{paddingTop: '20px'}}>
            <Row>
                <Col xs={9}>
                    <Form.Group className="mb-3" controlId="searchDrink.name" value = {drink.name} 
                        onChange={e => setDrink({name: e.target.value})}>
                        <Form.Control type="text" placeholder="Search" />
                    </Form.Group>    
                </Col>
                <Col>
                    <Button variant="primary" onClick={handleSubmit}>Search</Button>
                </Col>
                <Col/>
            </Row>
            <Row>
                {toggle && <DrinkTable drinks={search} deleteDrink={props.deleteDrink}/>}
            </Row>
        </Container>
    )
}

export default SearchDrink