import React from 'react'
import {GetDrinks, DeleteDrink, UpdateDrinkStock} from '../apiHandler'

import NewDrink from './newDrink'
import DrinkTable from './drinkTable'
import SearchDrink from './searchDrink'
import Container from 'react-bootstrap/Container'
import Alert from 'react-bootstrap/Alert'

function Drinks(){
    const [drinks, setDrinks] = React.useState([]);
    const [errors, setErrors] = React.useState([{}])
    const [show, setShow] = React.useState(true);

    React.useEffect(() => {
        const drinkAPI = GetDrinks();
        let mounted = true;
        drinkAPI.then((items) => {
            if (mounted) {
            setDrinks(items);
            }
        })
    }, []);

    const updateDrink = () => {
        const drinkAPI = GetDrinks();
        let mounted = true;
        drinkAPI.then((items) => {
            if (mounted) {
            setDrinks(items);
            }
        })
    }

    const handleUpdate = (slug, stock) => {
        UpdateDrinkStock(slug, stock)
    }

    const deleteDrink = (slug) => {
        const response = DeleteDrink(slug);
        response.then((result) => {
            if (!result.data.notice) {
                setErrors(result.data)
                setShow(true)
            }
            else {
                setDrinks(drinks.filter(item => item.attributes.slug !== slug))
                setErrors([{}])
            }
        })
    }

    return (
        <Container style={{paddingTop: '20px'}} >
            {errors.length > 1 && show &&
            <Alert variant='danger' onClose = {() => setShow(false)} dismissible>
                <Alert.Heading style={{fontSize: '15px'}}>{errors[0].error}</Alert.Heading>
            </Alert> 
            }
            <SearchDrink deleteDrink={deleteDrink}/>
            <Container>
                <NewDrink updateDrink={updateDrink}/>
            </Container>
            <DrinkTable drinks={drinks} deleteDrink={deleteDrink} handleUpdate={handleUpdate} del={true} />
        </Container>
        
    )

}

export default Drinks
