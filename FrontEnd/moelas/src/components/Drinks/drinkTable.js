import Table from 'react-bootstrap/Table'
import Button from 'react-bootstrap/Button'
import Container from 'react-bootstrap/Container'
import UpdateStock from './updateStock'


export const DrinkTable = (props) => {
    if (props.drinks != null && props.drinks.length !== 0)
    {
        return (
            <Container className="justify-content-md-center" style={{
                paddingTop: '20px',
                textAlign: 'center',
                }}>
                <Table hover>
                    <thead>
                        <tr>
                            <th>Image</th>
                            <th>Drink</th>
                            <th>Stock</th>
                            {props.del && <th>Delete</th>}
                            {props.del && <th>Update Stock</th>}
                        </tr>
                    </thead>
                    <tbody>
                        {props.drinks.map( drink => {
                            return(
                                <tr key={drink.id}>
                                    <td><img src={drink.attributes.img_url} width={40} height={40}/></td>
                                    <td>{drink.attributes.name}</td>
                                    <td>{drink.attributes.stock}</td>
                                    {props.del && <td>
                                        <Button variant="outline-danger" 
                                            onClick={() => props.deleteDrink(drink.attributes.slug)}>Delete</Button>
                                    </td>}
                                    {props.del && <td><UpdateStock drink={drink} handleUpdate={props.handleUpdate} /></td>}
                                </tr>
                            )
                        })}
                    </tbody>
                </Table>
            </Container>
        )
    }
        return <div>No drinks found</div>
    
}

export default DrinkTable