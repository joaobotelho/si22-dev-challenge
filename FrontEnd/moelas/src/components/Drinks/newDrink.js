import React from 'react'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import Container from 'react-bootstrap/Container'
import {CreateDrink} from '../apiHandler'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

export const NewDrink = (props) => {
    const [show, setShow] = React.useState(false);
    const [drink, setDrink] = React.useState({
        name: "",
        stock: 0,
        img_url: ""
    })

    const handleSubmit = () => {
        CreateDrink(drink.name, drink.stock, drink.img_url)
        props.updateDrink();
    }

    if (show) {
        return (
            <Container>
                <Form.Group className="mb-3" controlId="newDrink.Name"
                    value={drink.name}
                    onChange = {e => setDrink({name: e.target.value, stock: drink.stock, img_url: drink.img_url})}>
                    <Form.Label>Name</Form.Label>
                    <Form.Control type="text"/>
                </Form.Group>
                <Form.Group className="mb-3" controlId="newDrink.Stock"
                    name="stock" value = {drink.stock} 
                    onChange={e => setDrink({name: drink.name, stock: e.target.value, img_url: drink.img_url})}>
                    <Form.Label>Stock</Form.Label>
                    <Form.Control type="number"/>
                </Form.Group>
                <Form.Group className="mb-3" controlId="newDrink.img_url"
                    name="img_url" value = {drink.img_url} onChange={e => setDrink({
                        name: drink.name, stock: drink.stock, img_url: e.target.value
                    })}>
                    <Form.Label>URL</Form.Label>
                    <Form.Control type="text"/>
                </Form.Group>
                <Row>
                    <Col/>  
                    <Col>
                        <Button variant="primary" type="submit" onClick={handleSubmit}>Submit</Button>
                    </Col>
                    <Col xs={4} >
                        <Button variant="outline-primary" onClick = {() => setShow(false)}>Hide Form</Button> 
                    </Col>
                    <Col/>
                </Row>
            </Container>
        )
    }
    return <Container>
        <Row className="justify-content-md-left">
            <Button variant="outline-primary" onClick = {() => setShow(true)}> New Drink </Button>
        </Row>
        </Container>
}

export default NewDrink;