import Container from 'react-bootstrap/Container'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import { useState } from 'react'


export const UpdateStock = (props) => {
    const [stock, setStock] = useState(props.drink.attributes.stock);
    const slug = props.drink.attributes.slug

    return (
        <Container>
                <Form onSubmit={props.handleUpdate(slug, stock)}>
                    <Row>
                    <Col xs={8}>
                    <Form.Group className="mb-3" controlId="newStock" value = {stock} 
                        onChange={e => {
                            if (e.target.value < 0){
                                setStock(props.drink.attributes.stock);
                            }
                            else{
                                setStock(e.target.value)
                            }
                        }}>
                        <Form.Control type="number" placeholder="new stock" />
                    </Form.Group>
                    </Col>
                    <Col>
                        <Button variant="outline-success" type="submit">Update</Button>
                    </Col>
                    <Col/>
                    </Row>
                </Form>
        </Container>
    )
}

export default UpdateStock