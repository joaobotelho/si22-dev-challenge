import './App.css';
import Drinks from './components/Drinks/drinks'
import Cocktails from './components/Cocktails/cocktails'
import ErrorPage from './components/errorPage'
import React from 'react'

import {BrowserRouter as Router, Routes, Route} from 'react-router-dom'


function App(){

  return (
    <div className="App">
      <Router>
        <Routes>
          <Route path="/drinks" element={<Drinks />} />
          <Route path="/cocktails" element={<Cocktails />} />
          <Route path="*" element={<ErrorPage />} />
        </Routes>
      </Router>
    </div>
  )
}

export default App;