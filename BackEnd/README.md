# Backend

## Deciding on DataBase schema

It was decided to use the following tables for data management

| Cocktails |        |
| --------- | ------ |
| name      | string |
| slug      | string |

| Drinks  |        |
| ------- | ------ |
| name    | string |
| img_url | string |
| slug    | string |

Which were connected with a has_many through relationship, through the help of the table Ingredient, who also kept count of the amount of each drink used

| Ingredient  |         |
| ----------- | ------- |
| cocktail_id | integer |
| drink_id    | integer |
| amount      | integer |

To facilitate future searchs, both drinks and cocktails have a slug variable, which is their parameterized name.

At this point there were created some variables on the seed to facilitate testing.

## Creating the API

At first, it was created the basic CRUD functions, with a basic parameter check, and returning a serialization of the object.

Next, was created the Search and Buy functions. Search uses of the Query Interface to do a 'where like'
search, returning a json list. Buy does a stock check on the drinks needed for the cocktail, and returns a json notice on success.

At last, the create function on cocktail was updated to be able to add the ingredients simultaneously to its call.

## Testing

All functionalities of the API were tested through Postman, utilizing its results as support for the creation of the front-end.
