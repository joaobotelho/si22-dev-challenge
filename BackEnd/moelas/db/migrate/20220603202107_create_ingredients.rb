class CreateIngredients < ActiveRecord::Migration[7.0]
  def change
    create_table :ingredients do |t|
      t.integer :amount
      t.belongs_to :cocktail, null: false, foreign_key: true
      t.belongs_to :drink, null: false, foreign_key: true

      t.timestamps
    end
  end
end
