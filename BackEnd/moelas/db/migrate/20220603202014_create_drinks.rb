class CreateDrinks < ActiveRecord::Migration[7.0]
  def change
    create_table :drinks do |t|
      t.string :name
      t.string :img_url
      t.integer :stock
      t.string :slug
      t.timestamps
    end
  end
end
