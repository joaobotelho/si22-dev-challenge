# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#

albacora = Cocktail.create!(name: 'albacora')

coconut_milk = Drink.create!(name: 'coconut milk', stock: 50)
grenadine = Drink.create!(name: 'grenadine', stock: 100)
pineapple = Drink.create!(name: 'pineapple juice', stock: 60)

ingredients = Ingredient.create!([
    {
        cocktail: albacora, 
        drink: coconut_milk, 
        amount: 1,
    },
    {
        cocktail: albacora, 
        drink: grenadine,
        amount: 1,
    },
    {
        cocktail: albacora,
        drink: pineapple,
        amount: 10,
    },
])
