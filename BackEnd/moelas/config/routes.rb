Rails.application.routes.draw do
  root 'home#index'

  resources :cocktails, param: :slug

  resources :drinks, param: :slug

  resources :ingredients, only: [:create, :destroy, :update]
  
  # Re-routes all non_path routes to root

  get '/cocktails/buy/:slug', to: 'cocktails#buy'
  get 'cocktails/search/:search', to: 'cocktails#search'

  get '/drinks/search/:search', to: 'drinks#search'

  get '*path', to: 'home#index', via: :all
end
