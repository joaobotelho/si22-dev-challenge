class Drink < ApplicationRecord
    has_many :ingredients, dependent: :destroy
    has_many :cocktails, through: :ingredients

    before_create :slugify

    before_create :img_defaulf

    def img_defaulf
        if self.img_url.nil?
            self.img_url = ''
        end
    end

    def slugify
        self.slug = name.parameterize 
    end
end
