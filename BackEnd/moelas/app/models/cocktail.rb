class Cocktail < ApplicationRecord
    has_many :ingredients, dependent: :destroy
    has_many :drinks, through: :ingredients

    before_create :slugify

    def slugify
        self.slug = name.parameterize 
    end
end
