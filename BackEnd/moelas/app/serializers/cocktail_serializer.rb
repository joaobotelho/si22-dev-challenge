class CocktailSerializer
  include FastJsonapi::ObjectSerializer
  attributes :name, :slug

  has_many :ingredients
  has_many :drinks, through: :ingredients
end
