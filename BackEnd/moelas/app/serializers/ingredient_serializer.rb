class IngredientSerializer
  include FastJsonapi::ObjectSerializer
  attributes :amount, :cocktail_id, :drink_id
end
