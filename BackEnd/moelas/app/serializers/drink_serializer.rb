class DrinkSerializer
  include FastJsonapi::ObjectSerializer
  attributes 
end
class DrinkSerializer
  include FastJsonapi::ObjectSerializer
  attributes :name, :slug, :stock, :img_url

  has_many :ingredients
  has_many :cocktails, through: :ingredients
end
