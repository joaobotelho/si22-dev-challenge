class CocktailsController < ApplicationController
    protect_from_forgery with: :null_session

    before_action :set_cocktail, only: [:show, :edit, :update, :destroy, :buy]

    def index
        @cocktails = Cocktail.all.order(name: :asc)
        @cocktails_json = @cocktails.map do |cocktail|
            {
                :cocktail => cocktail, 
                :ingredients => 
                    cocktail.ingredients.map do |ingredient|
                        {
                            :drink => Drink.find(ingredient.drink_id).name,
                            :amount => ingredient.amount
                        }
                    end
            }
        end
        render json: @cocktails_json
    end

    def show
        render json: {cocktail: @cocktail, ingredients:
            @cocktail.ingredients.map do |ingredient|
                {
                    drink: Drink.find(ingredient.drink_id).name,
                    amount: ingredient.amount
                }
            end
        }
    end

    def create
        if Cocktail.find_by(slug: cocktail_params[:name].parameterize)
            render json: { errors: "Cocktail already exists"}
            return;
        end
        @cocktail = Cocktail.new(cocktail_params)
        if (params[:ingredients].nil?)
            render json: { errors: "No ingredient found"}
            return
        end
        params[:ingredients].map do |ingredient|
            @drink = Drink.find_by(slug: ingredient[:drink])
            if @drink.nil?
                render json: {errors: "Couldn't find drink"}
                return;
            end
            @new_ingredient = Ingredient.create!(
                {
                    cocktail: @cocktail,
                    drink: @drink,
                    amount: ingredient[:amount]
                }
            )
        end
        if @cocktail.save
            render json: CocktailSerializer.new(@cocktail).serialized_json
        else
            render json: { errors: @cocktail.errors.messages}
        end
    end

    def update
        if @cocktail.update(cocktail_params)
            render json: CocktailSerializer.new(@cocktail).serialized_json
        else
            render json: { errors: @cocktail.errors.messages}
        end
    end

    def edit
    end

    def destroy
        if @cocktail.destroy
            render json: {notice: 'Cocktail was successfully destroyed'}   
        else
            render json: {errors: @cocktail.errors.messages}
        end 
    end

    def search
        if params[:search].blank?
            render json: {notice: "No search params specified"}
            return
        end
        
        @parameter = Cocktail.sanitize_sql_like(params[:search].downcase)
        @match_cocktail = Cocktail.where("lower(name) LIKE :search", search: "%#{@parameter}%")
        if @match_cocktail.blank?
            render json: {notice: "No cocktail found"}
        else
            @cocktails_json = @match_cocktail.map do |cocktail|
                {
                    :cocktail => cocktail, 
                    :ingredients => 
                        cocktail.ingredients.map do |ingredient|
                            {
                                :drink => Drink.find(ingredient.drink_id).name,
                                :amount => ingredient.amount
                            }
                        end
                }
            end
            render json: @cocktails_json
        end
    end

    def buy
        if !@cocktail
            render json: {error: "Invalid cocktail."}
        end

        @cocktail.ingredients.each do |ingredient|
            if Drink.find(ingredient.drink_id).stock < ingredient.amount
                render json: {error: "Cannot buy drink, not enough ingredients"}
                return;
            end
        end

        @cocktail.ingredients.each do |ingredient|
            @drink = Drink.find(ingredient.drink_id)
            @drink.update(stock: @drink.stock - ingredient.amount)
        end

        render json: {notice: "Cocktail bought successfully."}
    end


    private

    def set_cocktail
        @cocktail = Cocktail.find_by(slug: params[:slug])
    end

    def cocktail_params 
        params.require(:cocktail).permit(:name, :ingredients => [])
    end

    def options 
        @options ||= { include: %i[ingredients drinks] }
    end
end