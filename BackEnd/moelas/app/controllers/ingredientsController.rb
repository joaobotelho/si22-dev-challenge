class IngredientsController < ApplicationController
    protect_from_forgery with: :null_session

    def create
        @ingredient = Ingredient.new(ingredient_params)

        if @ingredient.save
            render json: IngredientSerializer.new(@ingredient).serialized_json
        else
            render json: { error: @ingredient.errors.messages}, status: 422
        end
    end

    def update
        @ingredient = Ingredient.find(params[:id])
        if @ingredient.update(update_params)
            render json: IngredientSerializer.new(@ingredient).serialized_json
        else
            render json: { error: @ingredient.errors.messages}, status: 422
        end
    end

    def destroy
        @ingredient = Ingredient.find(params[:id])
        @cocktail = Cocktail.find(@ingredient.cocktail_id)
        if @cocktail.ingredients.length <= 2
            render json: {error: 'Cannot destroy ingredient, cocktail must have more or equal to 2 ingredients.'}
        elsif @ingredient.destroy
            render json: {notice: 'Ingredient was successfully destroyed'}
        else
            render json: { error: @ingredient.errors.messages}, status: 422
        end
    end

    private

    def ingredient_params
        params.require(:ingredient).permit(:amount, :drink_id, :cocktail_id)
    end

    def update_params
        params.require(:ingredient).permit(:amount)
    end
end