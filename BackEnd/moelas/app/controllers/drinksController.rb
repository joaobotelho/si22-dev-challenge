class DrinksController < ApplicationController
    protect_from_forgery with: :null_session

    before_action :set_drink, only: [:show, :edit, :update, :destroy]

    def index
        @drinks = Drink.all.order(name: :asc)
        render json: DrinkSerializer.new(@drinks).serialized_json
    end

    def show
        render json: DrinkSerializer.new(@drink).serialized_json
    end

    def create
        if Drink.find_by(slug: drink_params[:name].parameterize)
            render json: {error: "Drink already exists"}
            return;
        end
        @drink = Drink.new(drink_params)
        if @drink.save
            render json: DrinkSerializer.new(@drink).serialized_json
        else
            render json: {error: @drink.errors.messages}
        end
    end

    def update
        if @drink.update(drink_params)
            render json: DrinkSerializer.new(@drink).serialized_json
        else
            render json: {error: @drink.errors.messages}
        end
    end

    def search
        if params[:search].blank?
            render json: {notice: "No search params specified"}
            return
        end
        
        @parameter = Drink.sanitize_sql_like(params[:search].downcase)
        @match_drink = Drink.where("lower(name) LIKE :search", search: "%#{@parameter}%")
        if @match_drink.blank?
            render json: {notice: "No drink found"}
        else
            render json: DrinkSerializer.new(@match_drink).serialized_json
        end
    end

    def edit
    end

    def destroy
        if !@drink.cocktails.empty?
            render json: [
                {
                    error: "There are cocktails using this drink, delete cocktails first."
                },
                {
                    cocktails: CocktailSerializer.new(@drink.cocktails),
                }
            ]
        elsif @drink.destroy
            render json: {notice: 'Drink was successfully destroyed'}   
        else
             render json: {error: @drink.errors.messages}
        end
    end

    private

    def set_drink
        @drink = Drink.find_by(slug: params[:slug])
    end

    def drink_params 
        params.require(:drink).permit(:name, :stock, :img_url)
    end

    def options 
        @options ||= { include: %i[ingredients cocktails] }
    end
end